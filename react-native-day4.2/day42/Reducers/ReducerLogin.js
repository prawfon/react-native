const stateLogin = {
    users: "",
    password: "",
    FirstName: "",
    LastName: "",
    isLoading: false
};

export default (state = stateLogin, action) => {
    switch (action.type) {
        case "Login":
            return { users: action.username, password: action.password, isLoading: true };
        case "edit":
            return { users: action.username, FirstName: action.FirstName,LastName: action.LastName };
        default:
            return state;
    }
};
