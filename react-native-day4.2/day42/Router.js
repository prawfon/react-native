import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-native';
import { ConnectedRouter } from 'connected-react-router';
import {Provider} from 'react-redux';
import {store,history} from './store'

import Login from './Login';
import List from './List';
import Profile from './Profile';
import EditProfile from './EditProfile';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import Product from './Product';


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
            <ConnectedRouter history={history}>
                <Switch>
                    <Route exaat path="/Login" component={Login} />
                    <Route exact path="/List" component={List} />
                    <Route exact path="/Profile" component={Profile} />
                    <Route exact path="/EditProfile" component={EditProfile} />
                    <Route exact path="/AddProduct" component={AddProduct} />
                    <Route exact path="/EditProduct" component={EditProduct} />
                    <Route exact path="/Product" component={Product} />
                    <Redirect to="/Login" />
                </Switch>
            </ConnectedRouter>
            </Provider>
        );
    }
}

export default Router