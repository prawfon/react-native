/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity,Image } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class list extends React.Component {
    state = {
        modalVisible: false,
        view: ''
    };
    onShowModal(value) {
        this.setState({ modalVisible: true, view: value });
    }
    onHideModal() {
        this.setState({ modalVisible: false, view: '' });
    }

    render() {
        return (
            // <Modal
            // animationType="slide"
            // transparent={false}
            // visible={this.state.modalVisible}
            // onRequestClose={() => {
            //     Alert.alert('Modal has been closed.');
            // }} />

            <View style={styles.container} >
                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    transparent={true}
                    style={styles.modal}
                >
                    <TouchableOpacity
                        onPress={() => { this.onHideModal() }}
                        style={[styles.modal, styles.center]}
                    >
                        <Text style={styles.text}>{this.state.view}</Text>
                    </TouchableOpacity>

                </Modal>
                <View style={styles.header}>
                    <Text style={styles.headerText}>PRODUCT</Text>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                            <Image source={require('./top1.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                            <Image source={require('./top2.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                            <Image source={require('./top3.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                            <Image source={require('./top4.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                            <Image source={require('./top5.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                            <Image source={require('./top6.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: 'black',
        alignItems: 'center'
    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30

    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    photo: {
        // borderRadius: 30,
        width: 150,
        height: 180,
    },

    box1: {
        backgroundColor: 'white',
        flex: 1,
        margin: 14,
        // borderRadius: 20,
    },

    box2: {
        backgroundColor: 'white',
        flex: 1,
        margin: 14,
        // borderRadius: 20,
    },

    row: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
    },
    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    }

})
export default list
