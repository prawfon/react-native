/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput, Image, TouchableOpacity } from 'react-native';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
state = {
  user: " ",
  password: "",
}

  render() {
    return (
      <View style={styles.container}>
        <View>
        <Image
          source={require('./img/icon.png')}
        />
        </View>

        <TextInput style={[styles.button, { width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }]}
          onChangeText={(user) => this.setState({ user })}
          placeholder='username' />
          <Text>{this.state.user}</Text>
        
        <TextInput style={[styles.button, { width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }]}
          onChangeText={(password) => this.setState({ password })}
          placeholder='password' />
          <Text>{this.state.password}</Text>
        <TouchableOpacity >
          <Button title="TouchableOpacity"
            onPress={() => Alert.alert('Ask me later pressed', 'more thing..')}
          />
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingHorizontal: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    margin: 30
  },
});
