const ProductReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [{
                Product_id: action.Product_id,
                Productname: action.Productname,
                detail: action.detail,
                price: action.price
            }]
        case 'EDIT_PRODUCT':
            return [{
                ...state,
                ...action.payload
            }]

        default:
            return state
    }
}
const editProductAction = (Productname) => {
    return {
        type: "EDIT_PRODUCT",
        payload
    }
}
dispatch(editProductAction({
    Productname:''
}))

const productsReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, productReducer(null, action)]
        case 'EDIT_PRODUCT':
            return state.map((each) => {
                if (each.Productname === action.Productname) {
                    return productReducer(each, action)
                }
                return each
            })
        default:
            return state
    }
}