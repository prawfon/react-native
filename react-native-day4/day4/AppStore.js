import {createStore, combineReducers} from 'redux';
import TodoReducer from './TodoReducer';
import CompletesReucer from './CompletesReducer';
const reducers = combineReducers({
    todos: TodoReducer,
    completes: CompletesReucer
});

const store = createStore(reducers);
export default store;