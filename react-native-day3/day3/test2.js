import React, { Component } from 'react';
import { Text, View, Button, StyleSheet, TouchableOpacity } from 'react-native';


class test2 extends Component {
    state = {
        user: "",
        password: "",
    }

    goTotest1 = () => {
        this.props.history.push('/test1')
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.user && this.props.location.password)
            console.log(this.props.location)
        this.setState({ user: this.props.location.state.user, password: this.props.location.password })
    }
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.header}>
                    <Text style={styles.headerText}>My Profile</Text>
                </View>
                <View >
                    <Text style={styles.text}>Username: {this.state.user}</Text>
                </View>


                <View style={styles.content}>
                    <Button title="Edit" onPress={this.goTotest1}></Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'white',
        flex: 1
    },

    header: {
        backgroundColor: 'black',
        alignItems: 'center'
    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30

    },
    content: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'column',
        margin: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    }

})
export default test2
