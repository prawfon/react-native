import React, { Component } from 'react';
import { Text, View ,Button} from 'react-native';


class screen2 extends Component {
    goToScreen1 =()=>{
          this.props.history.push('/screen1',{
              mynumber:20
          })//ส่งข้อมูล=state กลับไป
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'red' }}>
                <Text style={{ color: 'white' }}>Screen2</Text>
               <Button title="Go to screen1" onPress={this.goToScreen1}></Button>
            </View>
        );
    }
}
export default screen2
