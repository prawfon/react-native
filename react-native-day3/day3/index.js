/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import screeen1 from './screeen1';
// import screeen2 from './screeen2';
import Router2 from './Router2';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router2);
