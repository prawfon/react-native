import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native';
import test1 from './test1';
import test2 from './test2';

class Router2 extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>

                    <Route exaat path="/test1" component={test1} />
                    <Route exact path="/test2" component={test2} />
                    <Redirect to="/test1" />
                </Switch>
            </NativeRouter>
        );
    }
}

export default Router2
