import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native';
import screen1 from './screen1';
import screen2 from './screen2';

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    
                    <Route exaat path="/screen1" component={screen1} />
                    <Route exact path="/screen2" component={screen2} />
                    <Redirect to="/screen1" />
                </Switch>
            </NativeRouter>
        );
    }
}

export default Router
