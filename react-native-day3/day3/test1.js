/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput, Image, TouchableOpacity } from 'react-native';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class test1 extends Component {
    goTotest2 = () => {
        this.props.history.push('/test2', {
            user: this.state.user, password: this.state.password
        })
    }
    state = {
        user: "",
        password: "",
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.content}>
                        <Image
                            source={require('./logo.png')} style={styles.photo}
                        />

                        <TextInput style={[styles.button, { width: 300, height: 40, borderColor: 'gray', borderWidth: 1, margin: 20 }]}
                            onChangeText={(user) => this.setState({ user })}
                            placeholder='username' />
                        <TextInput style={[styles.button, { width: 300, height: 40, borderColor: 'gray', borderWidth: 1, margin: 20 }]}
                            onChangeText={(password) => this.setState({ password })}
                            placeholder='password' />

                        <View style={styles.content}>
                            <Button title="Login"
                                onPress={this.goTotest2} />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingHorizontal: 20
    },
    content: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'column',
        margin: 30,
    },

    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },

    photo: {
        // borderRadius: 30,
        width: 350,
        height: 180,
    },
});
export default test1
