import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native';
import Login from './Login';
import List from './List';
import Profile from './Profile';
import EditProfile from './EditProfile';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import Product from './Product';

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exaat path="/Login" component={Login} />
                    <Route exact path="/List" component={List} />
                    <Route exact path="/Profile" component={Profile} />
                    <Route exact path="/EditProfile" component={EditProfile} />
                    <Route exact path="/AddProduct" component={AddProduct} />
                    <Route exact path="/EditProduct" component={EditProduct} />
                    <Route exact path="/Product" component={Product} />
                    <Redirect to="/Login" />
                </Switch>
            </NativeRouter>     
        );
    }
}

export default Router