const stateLogin = {
    users: "",
    password: "",
    firstName: "",
    lastName: "",
    isLoading: false
  };
  
  export default (state = stateLogin, action) => {
    switch (action.type) {
      case "Login":
        return { users: action.username,password: action.password, isLoading: true };
  
      default:
        return state;
    }
  };
  