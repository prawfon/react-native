/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import Login from './Login';
// import List from './List';
import Router from './Router';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router);
