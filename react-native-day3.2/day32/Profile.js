import React, { Component } from 'react';
import { Text, View, Button, StyleSheet, TouchableOpacity } from 'react-native';


class Profile extends Component {
    state = {
        user: "",
        password: "",
        FirstName: "",
        LastName: "",
    }
    goToList = () => {
        this.props.history.push('/List',
            {
                user: this.state.user,
                password: this.state.password,
                FirstName: this.state.FirstName,
                LastName: this.state.LastName
            }
        )
    }
    goToEditProfile = () => {
        this.props.history.push('/EditProfile',
            {
                user: this.state.user,
                password: this.state.password,
                FirstName: this.state.FirstName,
                LastName: this.state.LastName
            }
        )
    }
    componentDidMount() {
        this.setState({
            user: this.props.location.state.user,
            password: this.props.location.state.password,
            FirstName: this.props.location.state.FirstName,
            LastName: this.props.location.state.LastName
        })
    }

    UNSAFE_componentWillMount() {
        // console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.user && this.props.location.password
            && this.props.location.state.FirstName && this.props.location.state.LastName)
            console.log(this.props.location)
        this.setState({ user: this.props.location.state.user, password: this.props.location.password })
    }
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.header2}>
                    <View style={[styles.box3, styles.center]}><Text style={styles.text} onPress={this.goToList}>Back</Text></View>
                    <View style={[styles.box3, styles.center]}><Text style={styles.text}>My Profile</Text></View>

                </View>
                <View >
                    <Text style={styles.text}>Username: {this.state.user}</Text>
                    <Text style={styles.text}>First name:{this.state.FirstName} </Text>
                    <Text style={styles.text}>Last name:{this.state.LastName} </Text>
                </View>


                <View style={styles.content}>

                    <Button title="Edit" onPress={this.goToEditProfile}></Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'white',
        flex: 1
    },

    header: {
        backgroundColor: 'black',
        alignItems: 'center'
    },
    header2: {
        backgroundColor: 'black',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: -10
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30

    },
    content: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'column',
        margin: 20,///
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    box3: {
        backgroundColor: 'gray',
        flex: 1,
        margin: 2,
        padding: 10,
    }
})
export default Profile
