import React, { Component } from 'react';
import { Text, View, Button, StyleSheet, TouchableOpacity, TextInput } from 'react-native';


class AddProduct extends Component {
    state = {
        user: "",
        password: "",
        FirstName: "",
        LastName: "",
        ProductImage: "",
        ProductName: "",
    }

    goToList = () => {
        this.props.history.push('/List',
            {
                user: this.state.user,
                password: this.state.password,
                FirstName: this.state.FirstName,
                LastName: this.state.LastName,
                ProductImage: this.state.ProductImage,
                ProductName: this.state.ProductName,
            }
        )
    }
    goToProduct = () => {
        this.props.history.push('/Product',
            {
                ProductImage: this.state.ProductImage,
                ProductName: this.state.ProductName,
            }
        )
    }
    // componentDidMount() {
    //     this.setState({
    //         user: this.props.location.state.user,
    //         password: this.props.location.state.password,
    //         FirstName: this.props.location.state.FirstName,
    //         LastName: this.props.location.state.LastName,
    //         ProductImage: this.props.location.ProductImage,
    //         ProductName: this.props.location.ProductName,
    //     })
    // }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state && this.props.location.state.ProductImage && this.props.location.state.ProductName)
            console.log(this.props.location)
        this.setState({ ProductImage: this.props.location.state.ProductImage, ProductName: this.props.location.state.ProductName })
    }
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.header2}>
                    <View style={[styles.box3, styles.center]}><Text style={styles.text} onPress={this.goToList}>Back</Text></View>
                    <View style={[styles.box3, styles.center]}><Text style={styles.text}>Add Product</Text></View>

                </View>
                <View >

                    <TextInput style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1, margin: 20 }}
                        onChangeText={(ProductImage) => this.setState({ ProductImage })}
                        placeholder='Product Image' />
                    <TextInput style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1, margin: 20 }}
                        onChangeText={(ProductName) => this.setState({ ProductName})}
                        placeholder='Product Name' />
                </View>
                <View style={styles.content}>

                    <Button title="SAVE" onPress={this.goToProduct}></Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'white',
        flex: 1
    },

    header: {
        backgroundColor: 'black',
        alignItems: 'center'
    },
    header2: {
        backgroundColor: 'black',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: -10
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30

    },
    content: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'column',
        margin: 20,///
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    box3: {
        backgroundColor: 'gray',
        flex: 1,
        margin: 2,
        padding: 10,
    }
})
export default AddProduct
