import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Image, ScrollView } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class list extends Component {
    state = {
        user: "",
        password: "",
        FirstName: "",
        LastName: "",
        ProductImage: "",
        ProductName: "",
        modalVisible: false,
        view: ''
    }
    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToAddProduct = () => {
        this.props.history.push('/AddProduct',
            {
                user: this.state.user,
                password: this.state.password,
                FirstName: this.state.FirstName,
                LastName: this.state.LastName,
                ProductImage: this.state.ProductImage,
                ProductName: this.state.ProductName
            }
        )
    }
    goToProfile = () => {
        this.props.history.push('/Profile',
            {
                user: this.state.user,
                password: this.state.password,
                FirstName: this.state.FirstName,
                LastName: this.state.LastName,
            }
        )
    }
    // componentDidMount() {
    //     this.setState({
    //         user: this.props.location.state.user,
    //         password: this.props.location.state.password,
    //         FirstName: this.props.location.state.FirstName,
    //         LastName: this.props.location.state.LastName,
    //         ProductImage: this.props.location.ProductImage,
    //         ProductName: this.props.location.ProductName
    //     })
    // }
    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.user && this.props.location.password
            && this.props.location.state.FirstName && this.props.location.state.LastName && this.props.location.ProductImage && this.props.location.ProductName)
            console.log(this.props.location)
        this.setState({ user: this.props.location.state.user, password: this.props.location.password })
    }
    onShowModal(value) {
        this.setState({ modalVisible: true, view: value });
    }
    onHideModal() {
        this.setState({ modalVisible: false, view: '' });
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    transparent={true}
                    style={styles.modal}
                >
                    <TouchableOpacity
                        onPress={() => { this.onHideModal() }}
                        style={[styles.modal, styles.center]}
                    >
                        <Text style={styles.text}>{this.state.view}</Text>
                    </TouchableOpacity>

                </Modal>
                <View style={styles.header}>
                    <Text style={styles.headerText}>PRODUCT LIST</Text>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top1.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top2.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top3.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top4.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top5.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top6.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top7.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top8.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top9.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top10.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.box1, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...1') }}>
                                <Image source={require('./top11.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.box2, styles.center]}>
                            <TouchableOpacity onPress={(value) => { this.onShowModal(value = 'box...2') }}>
                                <Image source={require('./top12.jpg')} style={styles.photo} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.header2}>
                        <View style={[styles.box3, styles.center]}><Text style={styles.text} onPress={this.goToLogin}>L</Text></View>
                        <View style={[styles.box3, styles.center]}><Text style={styles.text} onPress={this.goToAddProduct}>Add</Text></View>
                        <View style={[styles.box3, styles.center]}><Text style={styles.text} onPress={this.goToProfile}>P</Text></View>
                    </View>
                </View>
            </ScrollView>

        );
    }
}

const styles = StyleSheet.create({

    contentContainer: {
        paddingVertical: 20
    },
    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: 'black',
        alignItems: 'center',
    },
    header2: {
        backgroundColor: 'black',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: -10
    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30

    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    photo: {
        // borderRadius: 30,
        width: 150,
        height: 180,
    },

    box1: {
        backgroundColor: 'white',
        flex: 1,
        margin: 14,
        // borderRadius: 20,
    },

    box2: {
        backgroundColor: 'white',
        flex: 1,
        margin: 14,
        // borderRadius: 20,
    },
    box3: {
        backgroundColor: 'gray',
        flex: 1,
        margin: 2,
        padding: 10,
    },

    row: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
    },
    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    }

})
export default list
