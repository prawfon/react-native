/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Appscreen1 from './Appscreen1';
import Appscreen2 from './Appscreen2';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Appscreen2);
