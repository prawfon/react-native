import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Appscreen2 extends React.Component {
    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>0</Text></View>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>Text</Text></View>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>X</Text></View>
                </View>
                <View style={[styles.scooll,styles.center]}>
                    <Text style={styles.headerText}>scoll</Text>
                </View>
                
                <View style={styles.header}>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>I</Text></View>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>C</Text></View>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>O</Text></View>
                    <View style={[styles.box,styles.center]}><Text style={styles.headerText}>N</Text></View>
                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#C0392B',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: -10
    },

    headerText: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 20
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    box: {
        backgroundColor: 'salmon',
        flex: 1,
        margin: 2,
        padding:10,
    },


    row: {
        backgroundColor: 'powderblue',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },
    scooll:{
        flex:2,

    },
    center:{
        alignItems:"center",
        justifyContent:"center",
    }

})
export default Appscreen2