
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Appscreen1 extends React.Component {
    render() {
        return (

            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.content, styles.center]}>
                        <View style={[styles.img, styles.circle, styles.center]}>
                            <Text style={styles.text}>Image</Text>
                        </View>
                    </View>

                    <View style={styles.content}>
                        <View style={[styles.row, styles.center]}>

                            <Text style={styles.text}>TextInput</Text>
                        </View>
                        <View style={[styles.row, styles.center]}>

                            <Text style={styles.text}>TextInput</Text>
                        </View>
                        <View style={[styles.Tochable, styles.center]}>

                            <Text style={styles.text}>TochableOpacity</Text>
                        </View>

                    </View>
                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'gray',
        flex: 1
    },

    img: {
        backgroundColor: '#F8C471',
        margin: 50,
    },

    circle: {
        borderRadius: 100,
        width: 200,
        height: 200,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        margin:6,
        padding: 12,
    },
    content: {
        backgroundColor: 'gray',
        flex: 1,
        flexDirection: 'column'
    },

    Tochable: {
        backgroundColor: 'powderblue',
        flex: 1,
        margin: 70,
    },

    row: {
        backgroundColor: 'powderblue',
        flex: 1,
        margin: 8,
        padding: 16,
    }


})
export default Appscreen1