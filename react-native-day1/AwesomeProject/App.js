// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  * @lint-ignore-every XPLATJSCOPYRIGHT1
//  */


import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class App extends React.Component {
    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <Text style={styles.headerText}>News</Text>
                </View>

                <View style={styles.content}>


                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text>box1</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text>box2</Text>
                        </View>
                        <View style={styles.box3}>
                            <Text>box3</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text>box1</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text>box2</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text>box1</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text>box2</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text>box1</Text>
                        </View>
                        {/* <View style={styles.box2}>
                            <Text>box2</Text>
                        </View>
                        <View style={styles.box3}>
                            <Text>box3</Text>
                        </View> */}
                    </View>

                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#C0392B',
        alignItems: 'center'
    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        backgroundColor: 'salmon',
        flex: 1,
        margin: 14
    },

    box2: {
        backgroundColor: 'yellow',
        flex: 1,
        margin: 14
    },
    box3: {
        backgroundColor: 'purple',
        flex: 1,
        margin: 14
    },

    row: {
        backgroundColor: 'powderblue',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    }

})
export default App